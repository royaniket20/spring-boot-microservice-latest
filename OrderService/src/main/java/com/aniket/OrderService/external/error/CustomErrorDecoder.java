package com.aniket.OrderService.external.error;

import com.aniket.OrderService.error.ErrorMessage;
import com.aniket.OrderService.error.OrderException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;

import java.io.IOException;

public class CustomErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String message, Response response) {
        try {
            ErrorMessage errorMessage = new ObjectMapper().readValue(response.body().asInputStream() , ErrorMessage.class);
            return new OrderException(errorMessage.getMessage() , errorMessage.getErrorCode());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
