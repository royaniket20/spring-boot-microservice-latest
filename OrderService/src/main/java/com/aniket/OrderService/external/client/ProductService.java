package com.aniket.OrderService.external.client;

import com.aniket.OrderService.config.FeignConfiguration;
import com.aniket.OrderService.external.model.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

//@FeignClient(name = "PRODUCT-SERVICE/product" )
@FeignClient(name = "API-GATEWAY/product" )
public interface ProductService {

    @PatchMapping("/modification/quantity/{id}")
    ResponseEntity<Product> reduceProductQuantityByProductId(@PathVariable("id")String id , @RequestParam @NotNull Long reducedQuantity);

    @GetMapping("/{id}")
    ResponseEntity<Product> getProductById(@PathVariable("id")String id);
}
