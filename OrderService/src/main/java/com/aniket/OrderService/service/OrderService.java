package com.aniket.OrderService.service;


import com.aniket.OrderService.model.Order;

import java.util.Map;

public interface OrderService {
    String createOrder(Order order);

    Order getOrderById(String id);

    Order patchOrderById(String id, Map<String, Object> attributes);
}
