package com.aniket.OrderService.service;

import com.aniket.OrderService.constant.ErrorCode;
import com.aniket.OrderService.constant.OrderStatus;
import com.aniket.OrderService.constant.PaymentMode;
import com.aniket.OrderService.entity.OrderEntity;
import com.aniket.OrderService.error.OrderException;
import com.aniket.OrderService.external.client.ProductService;
import com.aniket.OrderService.external.model.Product;
import com.aniket.OrderService.model.Order;
import com.aniket.OrderService.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductService productService;

    @Override
    public String createOrder(Order order) {
        log.info("Creating Order : {}", order);
        Product product = productService.reduceProductQuantityByProductId(order.getProductId() , order.getOrderQuantity()).getBody();
        log.info("Product Updated - {}",product);
        OrderEntity orderEntity = OrderEntity.builder()
                .productId(order.getProductId())
                .orderAmount(calculateOrderAmount(order.getProductId(), order.getOrderQuantity()))
                .orderDate(new Date())
                .orderStatus(OrderStatus.CREATED.name())
                .orderQuantity(order.getOrderQuantity())
                .paymentMode(order.getPaymentMode().name())
                .build();
        OrderEntity entity = orderRepository.save(orderEntity);
        log.info("Order Created : {}", entity);

        //Now Block Product in Product Service

        //Initiate Payment
        //Once payment Successful - Order Completed
        //If payment Failed - Update Order Failed - Update Product Unblock
        return entity.getOrderId()+"__"+product.getProductId();
    }

    private Double calculateOrderAmount(String productId, Long orderQuantity) {
        Product productFetched = productService.getProductById(productId).getBody();
        log.info("Product Received - {}",productFetched);
        return productFetched.getPrice()*orderQuantity;
    }

    @Override
    public Order getOrderById(String id) {
        log.info("Get the Order for Id - {}", id);
        OrderEntity orderEntity = orderRepository.findById(id).orElseThrow(() -> new OrderException("Order Not Found!!", ErrorCode.ORDER_NOT_FOUND));
        Order order = getOrder(orderEntity);
        log.info("Order fetched - {}", order);
        return order;
    }

    private static Order getOrder(OrderEntity orderEntity) {
        Order order = new Order();
        BeanUtils.copyProperties(orderEntity, order);
        order.setPaymentMode(PaymentMode.get(orderEntity.getPaymentMode()));
        order.setOrderStatus(OrderStatus.get(orderEntity.getOrderStatus()));
        return order;
    }

    @Override
    @Transactional
    public Order patchOrderById(String id, Map<String, Object> attributes) {
        OrderEntity orderEntity = orderRepository.findById(id).orElseThrow(() -> new OrderException("Order Not Found!!", ErrorCode.ORDER_NOT_FOUND));
        if (CollectionUtils.isEmpty(attributes)) {
            throw new OrderException("No Data Given for patching Order", ErrorCode.NO_DATA);
        } else {
            OrderEntity finalOrderEntity = orderEntity;
            attributes.entrySet().forEach(item -> {
                Field field = ReflectionUtils.findField(OrderEntity.class, item.getKey());
                field.setAccessible(true);
                Object value = item.getValue();
                if(!field.getType().equals(item.getValue().getClass()))
                {
                    log.info("Field Type ---- {} | Input Type --- {}", field.getType().getName(), value.getClass().getName());
                    String type = field.getType().getName();
                    switch (type){
                        case "java.lang.Long" :
                            value = Long.valueOf(String.valueOf(value));
                            break;
                        case "java.util.Date" :
                            try {
                                value = new SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(value));
                            } catch (ParseException e) {
                                throw new OrderException("Invalid Date format - Correct Format - yyyy-MM-dd" ,ErrorCode.HTTP_BAD_REQUEST);
                            }
                            break;
                        default:
                            throw new OrderException("Invalid input Key present" ,ErrorCode.HTTP_BAD_REQUEST);
                    }
                }

                ReflectionUtils.setField(field, finalOrderEntity,value );
            });
            //Just in Case someone trying to Update Id - Prevent it
            finalOrderEntity.setOrderId(id);
            orderEntity = orderRepository.save(finalOrderEntity);
        }
        Order order = getOrder(orderEntity);
        log.info("Order Patched - {}", order);
        return order;
    }

}
