package com.aniket.OrderService.entity;

import com.aniket.OrderService.constant.OrderStatus;
import com.aniket.OrderService.constant.PaymentMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
@Table(name = "PRODUCT_ORDER")
public class OrderEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "ORDER_ID")
    private String orderId;

    @Column(name = "PRODUCT_ID")
    private String productId;

    @Column(name = "ORDER_STATUS")
    private String orderStatus;
    @Column(name = "ORDER_QUANTITY")
    private Long orderQuantity;
    @Column(name = "ORDER_AMOUNT")
    private Double orderAmount;

    @Column(name = "ORDER_DATE")
    private Date orderDate;

    @Column(name = "ORDER_PAYMENT_MODE")
    private String paymentMode;
}
