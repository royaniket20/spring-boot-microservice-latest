package com.aniket.OrderService.controller;

import com.aniket.OrderService.model.Order;
import com.aniket.OrderService.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

    @Autowired
    private OrderService orderService;
     @PostMapping
    public ResponseEntity<String> createOrder(@RequestBody Order order){
        String productId = orderService.createOrder(order);
       return  new ResponseEntity<>(productId,HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable("id")String id){
         Order order = orderService.getOrderById(id);
        return  new ResponseEntity<>(order,HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Order> patchOrderById(@PathVariable("id")String id ,@RequestBody Map<String,Object> attributes){
        Order order = orderService.patchOrderById(id , attributes);
        return  new ResponseEntity<>(order,HttpStatus.OK);
    }
}
