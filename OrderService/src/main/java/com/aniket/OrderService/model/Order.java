package com.aniket.OrderService.model;

import com.aniket.OrderService.constant.OrderStatus;
import com.aniket.OrderService.constant.PaymentMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Order {

    private String orderId;


    private String productId;


    private OrderStatus orderStatus;

    private Long orderQuantity;

    private Double orderAmount;
    private Date orderDate;
    private PaymentMode paymentMode;
}
