package com.aniket.OrderService.constant;

import com.aniket.OrderService.error.OrderException;

import java.util.Arrays;

public enum PaymentMode {
    CASH,
    CREDIT_CARD,
    PAYPAL,
    CHEQUE,
    DEBIT_CARD,
    UPI,
    NET_BANKING;


    public static PaymentMode get(String value) {
        return Arrays.stream(PaymentMode.values())
                .filter(item -> item.name().equals(value))
                .findFirst().orElseThrow(()->new OrderException("Enum Value PaymentMode Not Found for attr : "+value , ErrorCode.ENUM_NOT_FOUND));
    }
}
