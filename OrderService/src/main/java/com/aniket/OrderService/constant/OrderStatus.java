package com.aniket.OrderService.constant;

import com.aniket.OrderService.error.OrderException;

import java.util.Arrays;

public enum OrderStatus {
    CREATED,
    FAILED,
    SUCCESSFUL,
    CANCELLED;

    public static OrderStatus get(String value) {
        return Arrays.stream(OrderStatus.values())
                .filter(item -> item.name().equals(value))
                .findFirst().orElseThrow(()->new OrderException("Enum Value OrderStatus Not Found for attr : "+value , ErrorCode.ENUM_NOT_FOUND));
    }
}
