package com.aniket.OrderService.error;

import com.aniket.OrderService.constant.ErrorCode;
import lombok.Data;

@Data
public class OrderException extends  RuntimeException {
    private ErrorCode erroCode;
    public OrderException(String message , ErrorCode erroCode) {
        super(message);
        this.erroCode=erroCode;
    }

}
