package com.aniket.ApiGateway.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallbackController {

    @RequestMapping("/orderServiceFallBack")
    public String orderServiceFallBack(){
        return "Order Service is Down";
    }
}
