package com.aniket.ApiGateway;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.circuitbreaker.resilience4j.ReactiveResilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.gateway.filter.ratelimit.RedisRateLimiter;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Mono;

import java.time.Duration;

@SpringBootApplication
public class ApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}


	@Bean
	KeyResolver userKeyResolver() {
		return exchange -> Mono.just("1");
	}


	private static final Duration TIMEOUT = Duration.ofSeconds(5);

	@Bean
	public Customizer<ReactiveResilience4JCircuitBreakerFactory> defaultCustomizer() {
		return factory -> factory.configureDefault(id -> new Resilience4JConfigBuilder(id)
				.circuitBreakerConfig(CircuitBreakerConfig.custom()
						.slidingWindowSize(20)
						.permittedNumberOfCallsInHalfOpenState(5)
						.failureRateThreshold(50)
						.waitDurationInOpenState(Duration.ofSeconds(60))
						.build())
				.timeLimiterConfig(TimeLimiterConfig.custom()
						.timeoutDuration(TIMEOUT)
						.build())
				.build()
		);
	}
	/*@Bean
	public RedisRateLimiter rateLimiter(){
		return new RedisRateLimiter(10,20,1);
	}*/

}
