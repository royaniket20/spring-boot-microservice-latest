package com.aniket.ProductService.controller;

import com.aniket.ProductService.model.Product;
import com.aniket.ProductService.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Map;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;
     @PostMapping
    public ResponseEntity<String> addProduct(@RequestBody Product product){
        String productId = productService.addProduct(product);
       return  new ResponseEntity<>(productId,HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id")String id){
         Product product = productService.getProductById(id);
        return  new ResponseEntity<>(product,HttpStatus.OK);
    }

    @PatchMapping("/modification/quantity/{id}")
    public ResponseEntity<Product> reduceProductQuantityByProductId(@PathVariable("id")String id , @RequestParam @NotNull Long reducedQuantity)
    {
        Product product = productService.reduceProductQuantityByProductId(id ,reducedQuantity);
        return  new ResponseEntity<>(product,HttpStatus.OK);
    }
}
