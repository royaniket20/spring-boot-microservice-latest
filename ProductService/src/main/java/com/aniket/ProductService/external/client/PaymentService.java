package com.aniket.ProductService.external.client;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

@CircuitBreaker(name = "paymentService", fallbackMethod = "paymentFallback")
//@Retry(name = "paymentService",fallbackMethod = "paymentFallback")
//@FeignClient(name = "PAYMENT-SERVICE/payment" )
@FeignClient(name = "API-GATEWAY/payment" )
public interface PaymentService {

        @PostMapping("/{id}")
        public ResponseEntity<String> makePayment(@PathVariable String id);


        default ResponseEntity<String> paymentFallback(Exception e){
                return new ResponseEntity<>("FALLBACK-ID"+ UUID.randomUUID().toString(), HttpStatus.OK);
        }
}
