package com.aniket.ProductService.service;

import com.aniket.ProductService.entity.ProductEntity;
import com.aniket.ProductService.constant.ErrorCode;
import com.aniket.ProductService.error.ProductException;
import com.aniket.ProductService.external.client.PaymentService;
import com.aniket.ProductService.model.Product;
import com.aniket.ProductService.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class ProductServiceImpl implements  ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PaymentService paymentService;
    @Override
    public String addProduct(Product product) {
        log.info("Creating Product : {}",product);
        ProductEntity productEntity = ProductEntity.builder()
                .productName(product.getProductName())
                .quantity(product.getQuantity())
                .price(product.getPrice())
                .build();
        ProductEntity entity = productRepository.save(productEntity);
        log.info("Product Created : {}",entity);
        return entity.getProductId();
    }

    @Override
    public Product getProductById(String id) {
        log.info("Get the Product for Id - {}",id);
        ProductEntity productEntity = productRepository.findById(id).orElseThrow(()-> new ProductException("Product Not Found!!" , ErrorCode.PRODUCT_NOT_FOUND));
        Product product = getProduct(productEntity);
        return  product;
    }

    private static Product getProduct(ProductEntity productEntity) {
        Product product = new Product();
        BeanUtils.copyProperties(productEntity,product);
        return product;
    }

    @Override
    public Product reduceProductQuantityByProductId(String id, Long reducedQuantity) {
        log.info("Reducing Product id : {} by Quantity - {}",id , reducedQuantity);
        ProductEntity productEntity = productRepository.findById(id).orElseThrow(()-> new ProductException("Product Not Found!!" , ErrorCode.PRODUCT_NOT_FOUND));
        if(productEntity.getQuantity()-reducedQuantity<0){
            throw  new ProductException("Not Much Stock Left" , ErrorCode.PRODUCT_QUANTITY_NOT_SUFFICIENT);
        }else{
            productEntity.setQuantity(productEntity.getQuantity()-reducedQuantity);
            productEntity = productRepository.save(productEntity);
        }
        //Now Lets Make payment -just a Dummy Call
        String paymentId = paymentService.makePayment(productEntity.getProductId()).getBody();
        log.info("Payment has been Made - {}",paymentId);
        Product product = getProduct(productEntity);
        product.setProductId(product.getProductId()+"__"+paymentId);
        return  product;
    }
}
