package com.aniket.ProductService.service;


import com.aniket.ProductService.model.Product;

public interface ProductService {
    String addProduct(Product product);

    Product getProductById(String id);

    Product reduceProductQuantityByProductId(String id, Long reducedQuantity);
}
