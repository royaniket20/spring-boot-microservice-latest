package com.aniket.ProductService.constant;

public enum ErrorCode {
    PRODUCT_NOT_FOUND, PRODUCT_QUANTITY_NOT_SUFFICIENT, INTERNAL_SERVER_ERROR, HTTP_BAD_REQUEST, ENUM_NOT_FOUND;
}
