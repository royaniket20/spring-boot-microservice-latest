package com.aniket.ProductService.constant;



import com.aniket.ProductService.error.ProductException;

import java.util.Arrays;

public enum PaymentMode {
    CASH,
    CREDIT_CARD,
    PAYPAL,
    CHEQUE,
    DEBIT_CARD,
    UPI,
    NET_BANKING;


    public static PaymentMode get(String value) {
        return Arrays.stream(PaymentMode.values())
                .filter(item -> item.name().equals(value))
                .findFirst().orElseThrow(()->new ProductException("Enum Value PaymentMode Not Found for attr : "+value , ErrorCode.ENUM_NOT_FOUND));
    }
}
