package com.aniket.ProductService.error;


import com.aniket.ProductService.constant.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ProductExceptionHandler {
    @ExceptionHandler(ProductException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleApplicationException(ProductException productException){
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(productException.getMessage())
                .errorCode(productException.getErroCode())
                .build();
        return errorMessage;
    }
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleRuntimeException(RuntimeException runtimeException ){
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(runtimeException.getMessage())
                .errorCode(ErrorCode.INTERNAL_SERVER_ERROR)
                .build();
        return errorMessage;
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage handleHttpRequestException(HttpMessageNotReadableException httpMessageNotReadableException){
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(httpMessageNotReadableException.getMessage())
                .errorCode(ErrorCode.HTTP_BAD_REQUEST)
                .build();
        return errorMessage;
    }
}
