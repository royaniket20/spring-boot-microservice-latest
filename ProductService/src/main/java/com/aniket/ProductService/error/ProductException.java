package com.aniket.ProductService.error;

import com.aniket.ProductService.constant.ErrorCode;
import lombok.Data;

@Data
public class ProductException extends  RuntimeException {
    private ErrorCode erroCode;
    public ProductException(String message , ErrorCode erroCode) {
        super(message);
        this.erroCode=erroCode;
    }

}
