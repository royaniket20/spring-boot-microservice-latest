package com.aniket.PaymentService.service;


import com.aniket.PaymentService.constant.OrderStatus;
import com.aniket.PaymentService.constant.PaymentMode;
import com.aniket.PaymentService.entity.PaymentEntity;
import com.aniket.PaymentService.external.client.ProductService;
import com.aniket.PaymentService.external.model.Product;
import com.aniket.PaymentService.repository.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private ProductService productService;
    @Override
    public String makePayment(String productId) {
        log.info("Making payment for a Product");
        log.info("Get the Product for Product Id - {}",productId);
        Product product = productService.getProductById(productId).getBody();
        log.info("Product fetched --- {}",product);
        PaymentEntity paymentEntity = PaymentEntity
                .builder()
                .orderId(UUID.randomUUID().toString())
                .paymentMode(PaymentMode.CHEQUE.name())
                .orderStatus(OrderStatus.SUCCESSFUL.name())
                .build();
        paymentEntity = paymentRepository.save(paymentEntity);
        log.info("Payment is Now successful -- {}",paymentEntity);
        return paymentEntity.getPaymentId();
    }
}
