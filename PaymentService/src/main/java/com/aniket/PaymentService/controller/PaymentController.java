package com.aniket.PaymentService.controller;


import com.aniket.PaymentService.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/payment")
@Slf4j
public class PaymentController {

    @Autowired
    private PaymentService paymentService;
    @PostMapping("/{id}")
    public ResponseEntity<String> makePayment(@PathVariable String id){
        String paymentId = paymentService.makePayment(id);
       return  new ResponseEntity<>(paymentId,HttpStatus.CREATED);
    }

}
