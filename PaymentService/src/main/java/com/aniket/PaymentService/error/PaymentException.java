package com.aniket.PaymentService.error;


import com.aniket.PaymentService.constant.ErrorCode;
import lombok.Data;

@Data
public class PaymentException extends  RuntimeException {
    private ErrorCode erroCode;
    public PaymentException(String message , ErrorCode erroCode) {
        super(message);
        this.erroCode=erroCode;
    }

}
