package com.aniket.PaymentService.error;



import com.aniket.PaymentService.constant.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class PaymentExceptionHandler {
    @ExceptionHandler(PaymentException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleApplicationException(PaymentException paymentException){
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(paymentException.getMessage())
                .errorCode(paymentException.getErroCode())
                .build();
        return errorMessage;
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleRuntimeException(RuntimeException runtimeException ){
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(runtimeException.getMessage())
                .errorCode(ErrorCode.INTERNAL_SERVER_ERROR)
                .build();
        return errorMessage;
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage handleHttpRequestException(HttpMessageNotReadableException httpMessageNotReadableException){
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(httpMessageNotReadableException.getMessage())
                .errorCode(ErrorCode.HTTP_BAD_REQUEST)
                .build();
        return errorMessage;
    }


}
