package com.aniket.PaymentService.error;


import com.aniket.PaymentService.constant.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ErrorMessage {
    private ErrorCode errorCode;
    private String message;
}
