package com.aniket.PaymentService.constant;


import com.aniket.PaymentService.error.PaymentException;

import java.util.Arrays;

public enum OrderStatus {
    CREATED,
    FAILED,
    SUCCESSFUL,
    CANCELLED;

    public static OrderStatus get(String value) {
        return Arrays.stream(OrderStatus.values())
                .filter(item -> item.name().equals(value))
                .findFirst().orElseThrow(()->new PaymentException("Enum Value OrderStatus Not Found for attr : "+value , ErrorCode.ENUM_NOT_FOUND));
    }
}
