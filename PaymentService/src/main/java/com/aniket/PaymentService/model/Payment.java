package com.aniket.PaymentService.model;


import com.aniket.PaymentService.constant.PaymentMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Payment {


    private String paymentId;
    private String orderStatus;
    private String orderId;
    private PaymentMode paymentMode;
}
