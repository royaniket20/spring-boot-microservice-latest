package com.aniket.PaymentService.external.client;

import com.aniket.PaymentService.external.model.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "PRODUCT-SERVICE/product" )
@FeignClient(name = "API-GATEWAY/product" )
public interface ProductService {
    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id")String id);
}
