package com.aniket.PaymentService.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
@Table(name = "PRODUCT_PAYMENT")
public class PaymentEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "PAYMENT_ID")
    private String paymentId;

    @Column(name = "PAYMENT_STATUS")
    private String orderStatus;

    @Column(name = "ORDER_PAYMENT_MODE")
    private String paymentMode;
    @Column(name = "ORDER_ID")
    private String orderId;
}
